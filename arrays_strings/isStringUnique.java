
import java.util.*;
import java.text.*;

public class isStringUnique {
	

	public static void main(String []args) {

		String str = args[0];

		isStringUnique isu = new isStringUnique();

		boolean isStrUnique = isu.isUniqueBasic(str);

		System.out.println(str + ": " + isStrUnique);
	}

	//ASCII or UNICODE
	private boolean isUnique(String str) {

		// HashMap<Character, Integer> map = new HashMap<Character, Integer>();
		HashSet<Character> set = new HashSet<Character>();

		StringCharacterIterator iter = new StringCharacterIterator(str);

		boolean duplicateFound = false;
		char c = iter.first();

		while(c != StringCharacterIterator.DONE && !duplicateFound) {

			// if(!map.containsKey(c)) {
			if(!set.contains(c)) {

				// map.put(c, 1);
				set.add(c);
			}
			else {
				duplicateFound = true;
			}

			c = iter.next();
		}

		return !duplicateFound;
	}

	private boolean isUniqueBasic(String str) {

		boolean duplicateFound = false;
		char c;

		for(int curr=0; curr < str.length() && !duplicateFound; curr++) {

			c = str.charAt(curr);

			for(int prev=0; prev < curr && !duplicateFound; prev++) {

				if(str.charAt(prev) == (c)) {

					duplicateFound = true;
				}
			}
		}

		return !duplicateFound;
	}
}
