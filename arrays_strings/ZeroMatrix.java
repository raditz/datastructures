
import java.lang.Math;
import java.util.*;

public class ZeroMatrix{

	public static void main(String[] args){

		ZeroMatrix sc = new ZeroMatrix();

		int [][]matr = {{1, 2, 3},
						{0, 2, 0},
						{1, 2, 3}};

		sc.nullifyMatrix(matr);

	}

	protected void nullifyMatrix(int [][]mat) {

		List<Integer> zeroRows = new ArrayList<Integer>(),
					  zeroColumns = new ArrayList<Integer>();

		for(int row=0; row< mat.length; row++) {
			for(int col=0; col< mat[0].length; col++) {

				if(mat[row][col] == 0) {

					if(!zeroRows.contains(row)) {
						System.out.println("row: " + row);
						zeroRows.add(row);
					}
					if(!zeroColumns.contains(col)) {
						System.out.println("col: " + col);
						zeroColumns.add(col);
					}
				}
			}
		}

		this.nullifyRows(zeroRows, mat);
		this.nullifyColumns(zeroColumns, mat);

		System.out.println(" 0,0" + mat[0][0]);
	}

	protected void nullifyRows(List<Integer> rows, int [][] mat) {

		for(Integer r : rows) {

			for(int col=0; col < mat[0].length; col++) {
				mat[r][col] = 0;
			}			
		}
	}

	protected void nullifyColumns(List<Integer> columns, int [][] mat) {

		for(Integer c : columns) {

			for(int row=0; row < mat.length; row++) {
				mat[row][c] = 0;
			}			
		}
	}
}
