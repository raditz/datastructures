
import java.util.*;

class Heap {
	

	public static void main(String []args) {

		Heap asa = new Heap();

		asa.addElement(4);
		asa.addElement(9);
		asa.addElement(3);
		asa.addElement(16);
		asa.addElement(12);

		asa.printArray();
		System.out.println(" size: " + asa.getSize());

		asa.heapSort(); //Check again
		System.out.println(" ");
		asa.printArray();
	}

	private int capacity = 10;
	private int size = 0;
	private int[] arr = new int[capacity];

	private int getLeftChildIndex(int index) { return index*2 + 1;}
	private int getRightChildIndex(int index) { return (index + 1)*2;}
	private int getParentIndex(int index) { return index == 0 ? -1 : (index - 1)/2;}

	private boolean hasLeft(int index) { return getLeftChildIndex(index) < size; }
	private boolean hasRight(int index) { return getRightChildIndex(index) < size; }
	private boolean hasParent(int index) { return getParentIndex(index) >= 0;}

	private int getLeftChild(int index) { return arr[getLeftChildIndex(index)]; }
	private int getRightChild(int index) { return arr[getRightChildIndex(index)]; }
	private int getParent(int index) { return arr[getParentIndex(index)]; }

	public int getSize() { return size; }

	protected void printArray() {

		for(int index = 0; index < size; index++) {
			System.out.print(" " + arr[index]);
		}
	}

	public int peek() {

		return arr[0];
	}

	public int poll() {

		int root = arr[0];

		arr[0] = arr[size-1];
		size--;

		heapifyDown();

		return root;
	}

	public void heapSort() {

		for(int pass = 1; pass < size; pass++) {

			swap(0, size - 1);
			heapifyUp();
		}
	}

	public void addElement(int ele) {

		arr[size] = ele;
		size++;

		heapifyUp();
	}

	private void heapifyDown() {

		int index = 0,
			smallerChildIndex = -1;
		boolean stop = false;

		while(!stop && hasLeft(index)) {

			smallerChildIndex = getLeftChildIndex(index);

			if(hasRight(index) && getRightChild(index) < arr[smallerChildIndex]) { //Get the smaller child
				smallerChildIndex = getRightChildIndex(index);
			}

			if(arr[smallerChildIndex] < arr[index]) { //Ideal case for ascending heap is val[parent] < val[child]
				swap(index, smallerChildIndex);
				index = smallerChildIndex;
			}
			else {
				stop = true;
			}
		}
	}

	private void heapifyUp() {

		int index = size - 1;

		int parentIndex = getParentIndex(index);
		System.out.println(" heapifyUp: " + index + " parentIndex: " + parentIndex);

		while(hasParent(index) && (getParent(index) > arr[index])) { //Ideal case for ascending heap is val[parent] < val[child]

			System.out.println(" heapifyUp Swapping: " + index + " parentIndex: " + getParentIndex(index) + " parent: " + getParent(index));

			swap(getParentIndex(index), index);
			index = getParentIndex(index);
		}
	}

	private void swap(int left, int right) {

		int tmp = arr[left];
		arr[left] = arr[right];
		arr[right] = tmp;
	}
}

