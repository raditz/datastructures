public class isSub {
    public boolean isSubsequence(String s, String t) {
        
        if(s.length() > t.length()){
            
            return false;
        }
        
        boolean [][] isSubSeq = new boolean[t.length()+1][s.length()+1];
        
        //Initialize 0th row and 0th column
        for(int currentCol=1; currentCol < s.length(); currentCol++){
            isSubSeq[0][currentCol] = false;
        }
        
        for(int currentRow=0; currentRow < t.length()+1; currentRow++){
            isSubSeq[currentRow][0] = true;
        }
        
        for(int currentRow=0; currentRow < t.length(); currentRow++){
            for(int currentCol=0; currentCol < s.length(); currentCol++){
            
            isSubSeq[currentRow+1][currentCol+1]
                = isSubSeq[currentRow][currentCol+1]
                    ||(isSubSeq[currentRow][currentCol]
                        &&(t.charAt(currentRow) == s.charAt(currentCol)));
            }
        }
        
        return isSubSeq[t.length()][s.length()];
    }
}

/*

C++ solution

class Solution {
public:
    bool isSubsequence(string s, string t) {
        return findSequence(s, 0, t, 0);
    }
private:
    bool findSequence(string& s, int s_index, string& t, int t_index){
        if(s_index == s.length())
            return true;
        if(t_index == t.length())
            return false;
            
        int index = t.find(s[s_index], t_index);
        if(index == string::npos)
            return false;
        else
            return findSequence(s, s_index + 1, t, index + 1);
    }
};
*/
