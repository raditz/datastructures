
#include <iostream>
#include <set>
#include <algorithm>
#include <iterator>
#include <vector>
using namespace std;

int p[7] = {30,35,15,5,10,20,25};


int main(void)
{

	cout << " bottom up memoization " << endl;

	vector<vector<int>> dp(7,vector<int>(7));
	vector<vector<int>> optimal_k(7,vector<int>(7));

	int n = 6;

	for(int l = 2; l <= n; l++){      //Chain Length

		cout << " l " << l << endl;

		for(int i = 1; i <= n-l+1; i++)   //i <= n always
		{
			cout << "i: " << i << endl;

			int j = i + l -1;

			dp[i][j] = 10000000;

			for(int k = i; k <= j-1; k++)
			{

				cout << "k: " << k << endl;
				int q = dp[i][k] + dp[k+1][j] + p[i-1]*p[k]*p[j];

				if(q < dp[i][j])
				{
					dp[i][j] = q;
					optimal_k[i][j] = k;

					cout << " K: " << k << " optimal_k: " << optimal_k[i][j] << " dp " << dp[i][j]  << endl;
				}
			}
		}
	}

	return 0;
}

