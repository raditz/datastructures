
import java.util.*;

class PaintFill{
	
	public static void main(String []args){

		int startRow = 2,
			startCol = 2;

		PaintFill pf = new PaintFill();
		pf.fillAllPaint(startRow, startCol);
	}

	private void fillAllPaint(int startRow, int startCol){

		boolean [][]grid = new boolean[4][5];
		List<Integer> rowPath = new ArrayList<Integer>(),
				  colPath = new ArrayList<Integer>();

		fillPaint(grid, startRow, startCol, rowPath, colPath);

		for(int path=0; path < rowPath.size(); path++){

			// System.out.println(" " + rowPath.get(path) + " " + colPath.get(path));
		}
	}

	private boolean fillPaint(boolean [][] grid,
						   int currentRow,
						   int currentColumn,
						   List<Integer> rowPath,
						   List<Integer> colPath){

		System.out.println(" currentRow: " + currentRow + " currentColumn: " + currentColumn);

		if(currentRow < 0 || currentColumn < 0 || currentRow > grid.length-1 || currentColumn > grid[0].length-1 || grid[currentRow][currentColumn]){
			System.out.println(" Ignored ");
			return false;
		}

		rowPath.add(currentRow);
		colPath.add(currentColumn);

		grid[currentRow][currentColumn] = true; 

		fillPaint(grid, currentRow-1, currentColumn,    rowPath, colPath);
		fillPaint(grid, currentRow+1, currentColumn,   rowPath, colPath);
		fillPaint(grid, currentRow,   currentColumn-1, rowPath, colPath);
		fillPaint(grid, currentRow,   currentColumn+1, rowPath, colPath);

		return true;
/*		if((currentRow-1 > -1)
			&& !grid[currentRow -1][currentColumn]){

			grid[currentRow-1][currentColumn] = true;
			rowPath.add(currentRow -1);
			colPath.add(currentColumn);

			fillPaint(grid, currentRow-1, currentColumn, rowPath, colPath);
		}

		if((currentRow+1 < 4)
			&& !grid[currentRow + 1][currentColumn]){

			grid[currentRow+1][currentColumn] = true;
			rowPath.add(currentRow + 1);
			colPath.add(currentColumn);

			fillPaint(grid, currentRow + 1, currentColumn, rowPath, colPath);
		}

		if((currentColumn-1 > -1)
			&& !grid[currentRow][currentColumn-1]){

			grid[currentRow][currentColumn-1] = true;
			rowPath.add(currentRow);
			colPath.add(currentColumn-1);

			fillPaint(grid, currentRow, currentColumn-1, rowPath, colPath);
		}

		if((currentColumn + 1 < 5)
			&& !grid[currentRow][currentColumn+1]){

			grid[currentRow][currentColumn + 1] = true;
			rowPath.add(currentRow);
			colPath.add(currentColumn + 1);

			fillPaint(grid, currentRow, currentColumn + 1, rowPath, colPath);
		}*/
	}
}
