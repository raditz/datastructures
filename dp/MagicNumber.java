
import java.lang.Math;

class MagicNumber{
	
	public static void main(String []args){

		int arraySize = Integer.valueOf(args[0]);

		System.out.println(arraySize);

		int []array = {-2, -1, 0, 2, 3, 5, 7, 9};

		MagicNumber M = new MagicNumber();

		int magicNumber = M.findMagicNumberForRepetitions(arraySize, array);

		System.out.println(magicNumber);
	}

	private int findMagicNumber(int arraySize, int []array){

		return this.findMagicNumberForIndices(array, 0, arraySize-1);
	}

	private int findMagicNumberForIndices(int []array, int startIndex, int endIndex){

		if(startIndex > endIndex){
			return -1;
		}

		int mid = (startIndex + endIndex)/2;

		if(array[mid] == mid){
			return mid;
		}
		else if(array[mid] > mid){
			return findMagicNumberForIndices(array, startIndex, mid-1);
		}
		else{
			return findMagicNumberForIndices(array, mid+1, endIndex);
		}
	}

	private int findMagicNumberForRepetitions(int arraySize, int []array){

		return this.findMagicNumberForIndicesRepititions(array, 0, arraySize-1);
	}

	private int findMagicNumberForIndicesRepititions(int []array, int startIndex, int endIndex){

		if(startIndex > endIndex){
			return -1;
		}

		int mid = (startIndex + endIndex)/2;

		if(array[mid] == mid){
			return mid;
		}
		int leftIndex = Math.min(array[mid], mid-1);
		int leftSearch = findMagicNumberForIndicesRepititions(array, startIndex, leftIndex);

		if(leftSearch != -1){
			return leftSearch;
		}

		int rightIndex = Math.max(array[mid], mid+1);
		int rightSearch = findMagicNumberForIndicesRepititions(array, rightIndex, endIndex);

		if(rightSearch != -1){
			return rightSearch;
		}

		return -1;
	}
}
