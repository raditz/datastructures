
import java.util.*;

class LargerSmallerNosWithSameBits {
	

	public static void main(String []args) {

		LargerSmallerNosWithSameBits asa = new LargerSmallerNosWithSameBits();

		int num = 3;
		/*num >>= 1;
		System.out.println("num: " + num);*/
		asa.largerNo(num);

	}

	protected void largerNo(int n) {

		int index = 0;
		int num = n;

		int lowerZero = -1,
			higherZero = -1,
			lowerOne = -1,
			higherOne = -1;

		boolean finish = false;

		while(!finish) {

			System.out.println("n: " + n);

			if((num & 1) != 0) {

				if(lowerOne == -1) {

					lowerOne = index;
				}
				else if(higherOne == -1) {

					higherOne = index;
				}
			}
			else {

				if(lowerZero == -1) {

					lowerZero = index;
				}
				else if(higherZero == -1) {

					higherZero = index;
				}
			}

			num >>= 1;

			finish = 
				(lowerZero != -1)
				&& (lowerOne != -1)
				&& (higherZero != -1)
				&& (higherOne != -1);
		}

		System.out.println(" higherOne: " + higherOne + " higherZero: " + higherZero + " lowerOne: " + lowerOne + " lowerZero: " + lowerZero);
	}
}
k