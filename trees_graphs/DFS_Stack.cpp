
#include<iostream>
#include<stack>

using namespace std;

class Node
{
public:
	int x,y, data;

	Node(int x_c, int y_c, int data_c)
	{x = x_c; y=y_c; data = data_c;}
};

void Print_Stack(stack<Node> &s)
{
	while( !s.empty() )
	{
		Node top = s.top();
		s.pop();
		cout << top.x << ' ' << top.y << ' ' << top.data << '\n';
	}
	cout << " Size is " << s.size() << '\n';
}


void Fill_Stack_using_DFS(stack<Node> &s, int arr[4][4], bool filled[4][4])
{
	int t = arr[0][0];
	s.push(0,0, t);
	
	while( !s.empty() )
	{
		Node top = s.top();
		s.pop();

		//Visit all the nodes do some process

		if(!filled[top.x][top.y])
		{
			filled[top.x][top.y] = true;

			s.push(top.x,top.y+1, arr[top.x][top.y+1]);
			s.push(top.x,top.y-1, arr[top.x][top.y-1]);
			s.push(top.x-1,top.y, arr[top.x-1][top.y]);
			s.push(top.x+1,top.y, arr[top.x+1][top.y]);
		}
	}
}


int main(void)
{
	stack<Node> s;
	
	//Node n(0,0,0);

	int arr1[4][4] = {{0,1,2,3}, {4,5,6,7}, {8,9,10,11}, {12,13,14,15}};

	bool fill[4][4];

	for(int if1 = 0; if1 < 4; if1++)
	{
		for(int jf1 = 0; jf1 < 4; jf1++)
		{
			fill[if1][jf1] = false;
		}
	}

	/*s.push(Node(0,0,0));
	s.push(Node(0,1,1));
	s.push(Node(0,2,2));*/

	Print_Stack(s);

	return 0;
}
