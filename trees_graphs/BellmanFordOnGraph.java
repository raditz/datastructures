
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.LinkedList;

public class BellmanFordOnGraph{
	

	public static void main(String []args){

		BellmanFordOnGraph bfs_g = new BellmanFordOnGraph();

		bfs_g.start();
	}

	protected void start(){

		this.bellManFordEdgeListGraph(0);
	}

	protected void bellManFordEdgeListGraph(int sourceVertex){

		boolean uSeenAlready = false;
		int currentVertex = -1;

		int distanceToVertex[] = {Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE};

		distanceToVertex[sourceVertex] = 0;

		//Iterations based on no. of vertices(n-1)
		for(int iteration = 0; iteration < 5; iteration++){

			for(int edge=0; edge < 6; edge++){

				int startVertex = edgeList[edge][0]; //This is true only for directed edges
				int endVertex   = edgeList[edge][1];
				int dist        = edgeList[edge][2];

				if((distanceToVertex[startVertex] < Integer.MAX_VALUE)
					&& distanceToVertex[endVertex] > distanceToVertex[startVertex] + dist){

					distanceToVertex[endVertex] = distanceToVertex[startVertex] + dist;
					System.out.println(" v: " + endVertex + " distanceToVertex[v]: " + distanceToVertex[endVertex]);
				}
			}
		}

		System.out.println(" d[4]: " + distanceToVertex[4]); //should be 40
	}


	protected	int [][]adjacencyMatrix = {{0, 20, 30, 40, -1},
									       {20, 0, -1, -1, 40},
									       {30, -1, 0, 10, 10},
									       {40, -1, 10, 0, 10},
									       {-1, 40, 10, 10, 0}};

	protected int [][] edgeList = {{0, 1, 20},
								   {1, 4, 40},
								   {0, 2, 30},
								   {0, 3, 40},
								   {3, 4, 10},
								   {2, 4, 10}};

	protected	int parentVertex[] = {-1, -1, -1, -1, -1};

}
