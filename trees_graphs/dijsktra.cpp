
#include <iostream>
#include <set>
#include <algorithm>
#include <iterator>
using namespace std;

int dist[8];

struct ltDist{
	bool operator()(int a, int b) const{
		return make_pair(dist[a], a) < make_pair(dist[b], b);
	}
};

int main(void)
{
	int graph[8][8];

	int n = 8;


	for(int i =0; i<8;i++)
	{
		for(int j =0; j<8;j++)
			graph[i][j]=-1;
	}

	for(int i =0; i<8;i++)
		dist[i] = 100;
dist[0] = 0;

	graph[0][1]=20;
	graph[0][3]=80;
	graph[0][6]=90;
	graph[1][5]=10;
	graph[2][7]=20;
	graph[2][3]=10;
	graph[2][5]=50;
	graph[3][6]=20;
	graph[3][2]=20;
	graph[4][1]=50;
	graph[4][6]=30;
	graph[5][2]=10;
	graph[6][0]=20;

	set<int, ltDist > s;

	s.insert(0);

	while(!s.empty())
	{
		int current_char = *s.begin();
		s.erase(s.begin());

		int u = current_char;
cout << " current : " << u << "\n";
		for(int neighbour = 0; neighbour<8; neighbour++)
		{
			if(graph[u][neighbour]!=-1)
			{
				if(dist[neighbour] > dist[u] + graph[u][neighbour])
				{
					if( s.count(neighbour) ) s.erase(neighbour);	//CONVERT TO CHAR
					dist[neighbour] = dist[u] + graph[u][neighbour];
					s.insert(neighbour);
					cout << " " << neighbour << " " << dist[neighbour] << "\n";
				}
			}
		}	
cout << "\n";	
	}

	//copy( dist, dist+5, cout);
	cout << " b dist : " << dist[5] << endl;

	return 0;
}

