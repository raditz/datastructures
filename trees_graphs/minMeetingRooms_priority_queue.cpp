/**
 * Definition for an interval.
 * struct Interval {
 *     int start;
 *     int end;
 *     Interval() : start(0), end(0) {}
 *     Interval(int s, int e) : start(s), end(e) {}
 * };
 */
class Solution {
public:
    int minMeetingRooms(vector<Interval>& intervals) {
        
        if(intervals.size() == 0) {
            
            return 0;
        }
        
        sort(intervals.begin(), intervals.end(), 
            [](Interval& left, Interval& right) {
            
                return left.start < right.start;
        });
        
        int count = 1;
        int lastEnd = intervals[0].end;
        
        auto comp = []( Interval a, Interval b ) { return a.end > b.end; };
        
        priority_queue<Interval, vector<Interval>, decltype(comp)> q(comp);
        
        q.push(intervals[0]);
        
//             intervals.begin(), intervals.begin(),
//             [ ](Interval left, Interval right) {
                
//                 return left.end < right.end;
//             });
             
        for(int interval = 1; interval < intervals.size(); interval++) {
            
            Interval earliestEnd = q.top();
            q.pop();
            
            if(intervals[interval].start >= earliestEnd.end) { //Incorrect since the next earliest end meeting might have finished
                
                earliestEnd.end = intervals[interval].end;
            }
            else {
                
                //lastEnd = min(lastEnd, intervals[interval].end);
                //++count;
                
                q.push(intervals[interval]);
            }
            
            q.push(earliestEnd);
        }
        
             
        return q.size();
    }
};